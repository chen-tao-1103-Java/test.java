package array;

import java.util.Arrays;

public class ArrayTest01 {

    public static void main(String[] args) {
        int[] array1 = {1, 2, 3};

        //for循环遍历数组
        for (int i = 0; i < array1.length; i++) {
            System.out.print(array1[i] + " ");
        }
        System.out.println();

        //  for each 增强for循环：   数组当中数据的类型定义的变量  : 数组名
        for (int x : array1) {
            System.out.print(x + " ");
        }
        System.out.println();


        //把数组转变为字符串，然后返回
        String ret = Arrays.toString(array1);
        System.out.println(ret);


        /*System.out.println(array1[1]);

        array1[1] = 99;

        System.out.println(array1[1]);*/

    }

    public static void main3(String[] args) {
        int[] array1 = {1, 2, 3};//直接赋值  静态初始化
        //             new type[] {dates};
        int[] array2 = new int[]{1, 2, 3, 4}; //动态初始化

        int[] array3 = new int[10];//只是分配了内存 但是没有进行赋值 只有默认值。

        int[] array4;
        array4 = new int[]{10, 20, 30};

        boolean[] flg = new boolean[10];

        char[] chars = new char[10];

        System.out.println("====");
    }


    public static void main2(String[] args) {


        //通过数组 来组织这一些数据
        int[] array = {1, 2, 3};
        float[] array2 = {1.0f, 2.5f};
        int ret = array[1];
        System.out.println(ret);

        int len = array.length;//数组名.长度
        System.out.println(len);
    }

    public static void main1(String[] args) {
        int[] array1 = {1, 2, 3, 4, 5};
        //遍历数组
        for (int i = 0; i < array1.length; i++) {
            System.out.print(array1[i] + " ");
        }
        System.out.println();


        //  数组当中数据的类型定义的变量  : 数组名    for each 增强for循环
        for (int x : array1) {
            System.out.print(x + " ");
        }
        System.out.println();


    }
}