package array;

import java.util.Arrays;

public class ArrayTest02 {
    public static void main(String[] args) {
        int[] array1 = {1, 2, 3, 4, 5};
        func1(array1);

        int[] array2 = {11, 22, 23, 24, 25};
        func2(array2);

        System.out.println(Arrays.toString(array1));//1 2 3 4
        System.out.println(Arrays.toString(array2));//99 22 23 24 25
    }


    public static void func1(int[] tmp1) {
        tmp1 = new int[10];
    }

    public static void func2(int[] tmp2) {
        tmp2[0] = 99;
    }
}

