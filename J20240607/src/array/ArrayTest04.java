package array;

import java.util.Arrays;


public class ArrayTest04 {
    public static void main(String[] args) {
        int[] array1 = {1, 3, 5, 7, 91, 11, 22, 44, 88, 18, 29, 17, 14};
        int[] array2 = array1.clone();//产生一个副本 Object里的方法
        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
        //Object
        array2[0] = 999;
        System.out.println("================");
        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
    }

    public static void main4(String[] args) {
        //调用API(copyOfRange)实现数组的范围拷贝：
        int[] array = {1, 3, 5, 7, 91, 11, 22, 44, 88, 18, 29, 17, 14};

        int[] array2 = Arrays.copyOfRange(array, 2, 5);//[2,5)

        System.out.println(Arrays.toString(array));

        System.out.println(Arrays.toString(array2));
    }


    //调用API(arraycopy)实现数组的拷贝：
    public static void main3(String[] args) {
        int[] array = {1, 3, 5, 7, 91, 11, 22, 44, 88, 18, 29, 17, 14};
        int[] copy = new int[array.length];
        //局部的拷贝
        System.arraycopy(array, 0, copy, 3, array.length - 3);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(copy));
    }


    //调用API(copyOf)实现数组的拷贝：
    public static void main2(String[] args) {
        int[] array = {1, 3, 5, 7, 91, 11, 22, 44, 88, 18, 29, 17, 14};

        //int[] array2 = Arrays.copyOf(array,array.length);
        //扩容 2倍
        int[] array2 = Arrays.copyOf(array, 2 * array.length);

        System.out.println(Arrays.toString(array));

        System.out.println(Arrays.toString(array2));
    }


    //自己实现数组的拷贝：
    public static void main1(String[] args) {
        int[] array1 = {1, 3, 5, 7, 91, 11, 22, 44, 88, 18, 29, 17, 14};
        int[] array2 = new int[array1.length];

        for (int i = 0; i < array1.length; i++) {
            array2[i] = array1[i];
        }

        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
    }
}
