package array;

import java.util.Arrays;

public class ArrayTest06 {
    public static void main(String[] args) {
        //数组逆置：
        int[] array = {1, 2, 3, 4, 5};
        String result = reverse(array);
        System.out.println(result);
    }

    public static String reverse(int[] array) {
        int left = 0;
        int right = array.length - 1;
        while (left < right) {
            int tmp = array[left];
            array[left] = array[right];
            array[right] = tmp;
            left++;
            right--;
        }
        return Arrays.toString(array);
    }
}
