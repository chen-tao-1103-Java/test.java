package array;

public class ArrayTest07 {
    public static void main(String[] args) {
        int[] array = {10, 20, 30, 50, 67};
        System.out.println(avg(array));
        System.out.println(find(array, 18));
    }

    //求平均数：
    public static double avg(int[] array) {
        int sum = 0;
        for (int x :
                array) {
            sum += x;
        }
        return sum * 1.0 / array.length;
    }

    //实现查找key的功能：
    public static int find(int[] array, int key) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                return i;
            }
        }
        return -1;
    }
}
