package array;

public class ArrayTest03 {
    public static void main(String[] args) {
        //这个不是拷贝因为没有产生新的内存空间

        int[] array1 = {1, 3, 5, 7, 9};
        int[] array2 = array1;
    }


/*
   自己实现toString()方法:
 */

    public static void main2(String[] args) {
        int[] array1 = null;
        //String ret = Arrays.toString(array1);
        //System.out.println(ret);
        String ret = myToString(array1);
        System.out.println(ret);
    }

    public static String myToString(int[] tmp) {
        if (tmp == null) {
            return "null";
        }
        String ret = "[";
        for (int i = 0; i < tmp.length; i++) {
            ret += tmp[i];
            if (i != tmp.length - 1) {
                ret += ",";
            }
        }
        ret += "]";
        return ret;
    }


    public static void main1(String[] args) {
        int[] array1 = {1, 2, 3, 4};
        System.out.println(array1.length);

        //array2这个引用变量指向{1,2,3,4}这个空间
        int[] array2 = array1;

        int[] array3 = null;
        //System.out.println(array3[0]);

        int[] array4 = new int[]{1, 2, 3, 4, 5};//开辟了一块新的内存


        int[] array5;
        array5 = new int[]{1, 2, 3, 4, 5};
    }
}
