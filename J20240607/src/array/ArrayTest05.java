package array;

import java.util.Arrays;

public class ArrayTest05 {
    public static void main(String[] args) {

        //调用API进行二分查找：
        int[] array = {1, 3, 5, 7, 91, 11, 22, 44, 88, 18, 29, 17, 14};
        Arrays.sort(array);// 底层快排
        //int index = binarySearch(array,3);
        int index = Arrays.binarySearch(array, 13);
        System.out.println(index);

    }


    public static void main1(String[] args) {
        //自己实现二分查找：
        int[] array = {59, 87, 86, 12, 26};
        binarySearch(array, 59);

    }

    public static int binarySearch(int[] array, int key) {
        int left = 0;
        int right = array.length - 1;
        while (left <= right) {
            int mid = (left + right) >>> 1;
            if (array[mid] < key) {
                left = mid + 1;
            } else if (array[mid] > key) {
                right = mid - 1;
            } else {
                return mid;
            }
        }
        return -1;
    }
}
