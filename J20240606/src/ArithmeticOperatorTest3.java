public class ArithmeticOperatorTest3 {
    public static void main(String[] args) {
        int a = 5;
        int b = 10;
        System.out.println(a + b);//15
        System.out.println(a - b); //-5
        System.out.println(a * b); //50

/*
  在数学运算中 5 / 10 = 0.5
  在Java中 5 / 10 的结果不是0.5，在java中有一个语法规则：
  int类型和int类型做运算,最终结果还是int类型，所以5 / 10
  的结果就是0，这个过程会取整数部分。
 */
        System.out.println(a / b); //0
//求余数：
        System.out.println(a % b); //5

/*
  ++和--都是单目运算符，++负责自加1，--负责自减
   ++无论是出现在变量前还是变量后，只要执行完++
   最终的结果都会让变量自加1
 */
        int i = 10;
        i++;
        System.out.println("i = " + i);//11
        int j = 10;
        ++j;
        System.out.println("j = " + j);//11


//在java语法规定，当++运算符出现在变量后（后置++），会先做赋值运算，再加1
//k = 11   l = 10
        int k = 10;
        int l = k++;
        System.out.println("k = " + k);//11
        System.out.println("l = " + l);

//java语法规定：++出现在变量前面（前置++）会先进行自加1，然后再进行赋值运算
//x = 11   y = 11
        int x = 10;
        int y = ++x;
        System.out.println("x = " + x); //11
        System.out.println("y = " + y); //11


        int c = 100;
        System.out.println(c++);
        System.out.println(c);
        int d = 100;
        System.out.println(++d);
        System.out.println(d);

//ArithmeticException
        System.out.println(3 / 0);
        System.out.println(10 % 0);
    }
}
