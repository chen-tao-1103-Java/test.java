package array;

public class ArrayTest {
    public static void main(String[] args) {
        //静态初始化int[]：
        int[] array1 = {1, 2, 3, 4, 5};
        //遍历数组：
        for (int i = 0; i < array1.length; i++) {
            System.out.print(array1[i] + " ");
        }
        System.out.println();

        for (int x : array1) {
//            System.out.print(array1[x] + " ");数组越界
            System.out.print(x + " ");
        }
        System.out.println();

        //静态初始化String[]
        String[] strings = {
                "三国演义",
                "水浒传",
                "西游记",
                "红楼梦"};

        System.out.println(strings.length);

        //遍历数组：
        for (int i = 0; i < strings.length; i++) {
            System.out.println(strings[i] + " ");
        }
        System.out.println();

        for (String s : strings) {
            System.out.println(s + " ");
        }
        System.out.println();

        //动态初始化数组：
        int[] array2 = new int[2];//打印默认值
        for (int i = 0; i < array2.length; i++) {
            System.out.print(array2[i] + " ");
        }
        System.out.println();

        String[] string = new String[1];
        for (int i = 0; i < string.length; i++) {
            System.out.println(string[i]);
        }

        //数组的的拷贝：
        int[] array3 = {1, 2, 3, 4, 5};
        int[] array4 = new int[array3.length * 2];
        System.arraycopy(array3, 0, array4, 0, array3.length);
        for (int x : array4) {
            System.out.print(x + " ");
        }
        System.out.println();
    }
}
