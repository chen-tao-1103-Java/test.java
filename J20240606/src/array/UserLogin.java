package array;

public class UserLogin {
    public static void main(String[] args) {
        if (args.length != 2) {
            throw new RuntimeException("请输入正确的参数");
        }

        //获取第一个元素为用户名：
        String userName = args[0];
        //获取第二个元素为密码：
        String passWord = args[1];

        //判断用户名和密码是否正确：
        if ("admin".equals(userName) && "123".equals(passWord)) {
            System.out.println("登录成功");
        } else {
            throw new RuntimeException("登录失败");
        }
    }
}
