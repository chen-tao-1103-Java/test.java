public class AssignmentOperatorTest1 {
    public static void main(String[] args) {
        //基本的赋值运算符：
        int a = 10;
        System.out.println("a = " + a);//10

        //扩展的赋值运算符：
        int b = 100;
        b += 10;
        System.out.println("b = " + b);//110

        int c = 10;
        c -= 10;
        System.out.println("x = " + c);//0

        int y = 20;
        y *= 2;
        System.out.println("y = " + y);//40

        int z = 100;
        z /= 10;
        System.out.println("z = " + z);//10

        int m = 6;
        m %= 3;
        System.out.println("m = " + m);//0


        byte x = 1;     //x+=1相当于 x = (byte)(x + 1);
        x += 1;
        System.out.println(x);//2
//        x = x + 1;//编译不通过

        //三目运算符
        int e = 10;
        int f = 10;
        System.out.println(x == y ? "x和y相等" : "x和y不相等");
    }
}
