public class ArithmeticOperatorTest2 {
    public static void main(String[] args) {
        //System.out.println(10 > 20 & 10 / 0 == 0);             // 程序抛出异常
        // System.out.println(10 < 20 | 10 > 0);             // 程序抛出异常
        System.out.println(-1 >>> 1);

        int a = 10;
        int b = 20;
        double c = a > b ? 1 : 2.0;
        b = 10;
    }

    public static void main1(String[] args) {
        System.out.println(5 > 3 & 6 > 5); //true
        System.out.println(10 > 15 | 10 < 12);//true
        System.out.println(!(3 > 2));//false
        System.out.println(true & false);//false
        System.out.println(true | false);//true
        System.out.println(true ^ false);//true
        System.out.println(!true);//false
    }
}
