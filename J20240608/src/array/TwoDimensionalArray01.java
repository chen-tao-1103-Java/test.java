package array;

public class TwoDimensionalArray01 {
    public static void main(String[] args) {
        int[][] array1 = {
                {10, 20, 30},
                {40, 50, 60},
                {70, 80, 90}
        };

        test1(array1);

        test2(new String[][]{
                {"我", "爱"},
                {"中", "国"},
                {"欢", "迎", "你"}
        });

        test3();
        test3(100);
        test3(100, 200, 300);

    }

    private static void test1(int[][] array) {
        System.out.println("test1()方法被调用");
    }

    private static void test2(String[][] array2) {
        System.out.println("test2()方法被调用");
    }


    /*
       可变长参数：可以接收0~N个参数,可变长参数只能有一个;可变长参数可以当作数组来处理

     */
    private static void test3(int... nums) {
//        System.out.println(nums.length);
        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i] + " ");
        }
        System.out.println();
    }
}
