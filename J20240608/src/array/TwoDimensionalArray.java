package array;

public class TwoDimensionalArray {
    public static void main(String[] args) {
        //静态初始化二维数组
        int[][] array1 = {
                {10, 20, 30},
                {40, 50, 60},
                {70, 80, 90}
        };

        //动态初始化二维数组
        int[][] array2 = new int[3][4];

        //访问二维数组中的元素
        System.out.println(array1[0][2]); //30
        System.out.println(array2[0][0]); //0

        //获取二维数组中元素的个数
        //array.length获取的是二维数组中有多少个一维数组
        System.out.println(array1.length);
        System.out.println(array2.length);

        //遍历二维数组
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1[i].length; j++) {
                System.out.print(array1[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("=========================");

        for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array2[i].length; j++) {
                System.out.print(array2[i][j] + " ");
            }
            System.out.println();
        }
    }
}
