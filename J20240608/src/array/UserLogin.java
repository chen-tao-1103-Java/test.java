package array;


/*
   测试 public static void main(String[] args)

    1：String[] args 是一个String 类型的一维数组
    2：(String[] args)是一个局部变量
    3：JVM准备一个String[] args = new {},调用main方法时传递实参args
    3：args.length 默认为0

    模拟用户通过命令行参数来登录,登录成功继续访问系统,登录失败无法使用系统
    步骤：1：java程序接收参数，并保存到args一维数组中，并将args传递给main()这一步由JVM来完成
         2：判断args一维数组中元素个数，如果不是2个参数登录失败
         3：如果元素个数是2，判断用户名和密码是否正确，不正确则登录失败

 */

public class UserLogin {
    public static void main(String[] args) {
        System.out.println(args.length); //默认为0
        if (args.length != 2) {
            throw new RuntimeException("请输入正确的参数");
        }

        //获取第一个元素为用户名：
        String userName = args[0];
        //获取第二个元素为密码：
        String passWord = args[1];

        //判断用户名和密码是否正确：
        if ("admin".equals(userName) && "123".equals(passWord)) {
            System.out.println("登录成功");
        } else {
            throw new RuntimeException("登录失败");
        }
    }
}
